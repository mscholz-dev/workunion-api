import request from "supertest";
import app from "../../../app.js";
import data from "../../config/data.js";

const route = "/api/desk/booking/date";

describe(`GET: ${route}`, () => {
  // id required return a 404

  it("it should throw: name does not exist", async () => {
    const res = await request(app).get(
      `${route}/invalid`,
    );

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "name does not exist",
    );
  });

  it("it should return bookings for marx", async () => {
    const res = await request(app).get(
      `${route}/marx`,
    );

    expect(res.statusCode).toBe(200);
    expect(res.body.bookings).toEqual([
      {
        start: data.startDateCoworking.getTime(),
        end: data.endDateCoworking.getTime(),
      },
      {
        start: data.startDateRent.getTime(),
        end: data.endDateRent.getTime(),
      },
      {
        start: data.startDateMeeting.getTime(),
        end: data.endDateMeeting.getTime(),
      },
    ]);
  });
});
