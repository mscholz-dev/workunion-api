import request from "supertest";
import app from "../../../app.js";
import data from "../../config/data.js";

const route = "/api/desk/booking";

describe(`POST: ${route}`, () => {
  it("it should throw: desk_name required", async () => {
    const res = await request(app).post(route);
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "desk_name required",
    );
  });

  it("it should throw: type required", async () => {
    const res = await request(app)
      .post(route)
      .send({ desk_name: data.deskName });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "type required",
    );
  });

  it("it should throw: type invalid", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: "location",
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe("type invalid");
  });

  it("it should throw: start required", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "start required",
    );
  });

  it("it should throw: start must be a number", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: "not a number",
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "start must be a number",
    );
  });

  it("it should throw: end required", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: new Date().getTime(),
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe("end required");
  });

  it("it should throw: end must be a number", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: new Date().getTime(),
        end: "not a number",
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "end must be a number",
    );
  });

  it("it should throw: last_name required", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: new Date().getTime(),
        end: new Date().getTime(),
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "last_name required",
    );
  });

  it("it should throw: last_name too long", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: new Date().getTime(),
        end: new Date().getTime(),
        last_name: data.string61,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "last_name too long",
    );
  });

  it("it should throw: first_name required", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: new Date().getTime(),
        end: new Date().getTime(),
        last_name: data.lastName,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "first_name required",
    );
  });

  it("it should throw: first_name too long", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: new Date().getTime(),
        end: new Date().getTime(),
        last_name: data.lastName,
        first_name: data.string61,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "first_name too long",
    );
  });

  it("it should throw: email required", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: new Date().getTime(),
        end: new Date().getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "email required",
    );
  });

  it("it should throw: email too long", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: new Date().getTime(),
        end: new Date().getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.string256,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "email too long",
    );
  });

  it("it should throw: email invalid", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: new Date().getTime(),
        end: new Date().getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: "not an email",
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "email invalid",
    );
  });

  it("it should throw: phone required", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: new Date().getTime(),
        end: new Date().getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "phone required",
    );
  });

  it("it should throw: phone invalid", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: new Date().getTime(),
        end: new Date().getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: "0000",
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "phone invalid",
    );
  });

  it("it should throw: company_siret invalid", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingRent,
        start: new Date().getTime(),
        end: new Date().getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
        company_siret: "0000",
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "company_siret invalid",
    );
  });

  it("it should throw: company_name required", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingRent,
        start: new Date().getTime(),
        end: new Date().getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "company_name required",
    );
  });

  it("it should throw: company_siret required", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingRent,
        start: new Date().getTime(),
        end: new Date().getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
        company_name: data.company,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "company_siret required",
    );
  });

  it("it should throw: start must be greater than or equal to tomorrow", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: new Date().getTime(),
        end: new Date().getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "start must be greater than or now + 2 days",
    );
  });

  it("it should throw: start must be lower than end", async () => {
    const sameDate = new Date();
    sameDate.setDate(sameDate.getDate() + 2);

    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: sameDate.getTime(),
        end: sameDate.getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "start must be lower than end",
    );
  });

  it("it should throw: start must be between 9h and 18h", async () => {
    const startDate = new Date();
    startDate.setDate(startDate.getDate() + 2);
    startDate.setHours(7);

    const endDate = new Date();
    endDate.setDate(endDate.getDate() + 2);

    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: startDate.getTime(),
        end: endDate.getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "start must be between 9h and 18h",
    );
  });

  it("it should throw: end must be between 9h and 18h", async () => {
    const endDate = new Date();
    endDate.setDate(endDate.getDate() + 2);
    endDate.setHours(19);

    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: data.startDateCoworking.getTime(),
        end: endDate.getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "end must be between 9h and 18h",
    );
  });

  it("it should throw: desk_name not exists", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: "invalid desk_name",
        type: data.deskBookingCoworking,
        start: data.startDateCoworking.getTime(),
        end: data.endDateCoworking.getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "desk_name not exists",
    );
  });

  it("it should create a coworking desk booking", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start: data.startDateCoworking.getTime(),
        end: data.endDateCoworking.getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
      });

    expect(res.statusCode).toBe(200);
  });

  it("it should create a rent desk booking", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingRent,
        start: data.startDateRent.getTime(),
        end: data.endDateRent.getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
        company_name: data.company,
        company_siret: data.siret,
      });

    expect(res.statusCode).toBe(200);
  });

  it("it should create a meeting desk booking", async () => {
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingMeeting,
        start: data.startDateMeeting.getTime(),
        end: data.endDateMeeting.getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
      });

    expect(res.statusCode).toBe(200);
  });

  it("it should throw: date is taken", async () => {
    // equal start and end

    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingRent,
        start: data.startDateRent.getTime(),
        end: data.endDateRent.getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
        company_name: data.company,
        company_siret: data.siret,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "date is taken",
    );
  });

  it("it should throw: date is taken", async () => {
    // englobe existing date
    const res = await request(app)
      .post(route)
      .send({
        desk_name: data.deskName,
        type: data.deskBookingCoworking,
        start:
          data.startBadDateCoworking.getTime(),
        end: data.endBadDateCoworking.getTime(),
        last_name: data.lastName,
        first_name: data.firstName,
        email: data.email,
        phone: data.phone,
        company_name: data.company,
        company_siret: data.siret,
      });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "date is taken",
    );
  });
});
