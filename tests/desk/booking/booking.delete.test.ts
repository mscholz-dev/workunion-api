import request from "supertest";
import app from "../../../app.js";
import data from "../../config/data.js";

// types
import { TType } from "../../../src/utils/type.js";

const route = "/api/desk/booking";

let bookingsId = {
  COWORKING: "",
  RENT: "",
  MEETING: "",
};

describe(`DELETE: ${route}`, () => {
  it("it should get bookings id", async () => {
    const res = await request(app).get(
      `${route}/id/marx`,
    );

    expect(res.statusCode).toBe(200);
    expect(res.body.bookings.length).toBe(3);

    for (const { id, type } of res.body.bookings)
      bookingsId[type as TType] = id;
  });

  it("it should throw: id invalid", async () => {
    const res = await request(app).delete(
      `${route}/invalid`,
    );

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe("id invalid");
  });

  it("it should throw: desk_booking not found", async () => {
    const res = await request(app).delete(
      `${route}/${data.badObjectId}`,
    );

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "desk_booking not found",
    );
  });

  it("it should delete a coworking desk_booking", async () => {
    const res = await request(app).delete(
      `${route}/${bookingsId.COWORKING}`,
    );

    expect(res.statusCode).toBe(200);
  });

  it("it should delete a rent desk_booking", async () => {
    const res = await request(app).delete(
      `${route}/${bookingsId.RENT}`,
    );

    expect(res.statusCode).toBe(200);
  });

  it("it should delete a meeting desk_booking", async () => {
    const res = await request(app).delete(
      `${route}/${bookingsId.MEETING}`,
    );
    expect(res.statusCode).toBe(200);
  });
});
