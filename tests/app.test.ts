import request from "supertest";
import app from "../app.js";

const resetRoute = "/api/test/reset";

// desk booking
import "./desk/booking/booking.create.test.js";
import "./desk/booking/booking.get.test.js";
import "./desk/booking/booking.delete.test.js";

// contact
import "./contact/contact.create.test.js";

beforeAll(async () => {
  // reset test db
  await request(app).delete(resetRoute);
});

beforeEach(async () => {
  // delete cache for every test
  jest.resetModules();
});
