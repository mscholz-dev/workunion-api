import request from "supertest";
import app from "../../app.js";
import data from "../config/data.js";

const route = "/api/contact";

describe(`POST: ${route}`, () => {
  it("it should throw: name required", async () => {
    const res = await request(app).post(route);
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "name required",
    );
  });

  it("it should throw: name too long", async () => {
    const res = await request(app)
      .post(route)
      .send({ name: data.string61 });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "name too long",
    );
  });

  it("it should throw: email required", async () => {
    const res = await request(app)
      .post(route)
      .send({
        name: `${data.lastName} ${data.firstName}`,
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "email required",
    );
  });

  it("it should throw: email too long", async () => {
    const res = await request(app)
      .post(route)
      .send({
        name: `${data.lastName} ${data.firstName}`,
        email: data.string256,
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "email too long",
    );
  });

  it("it should throw: email invalid", async () => {
    const res = await request(app)
      .post(route)
      .send({
        name: `${data.lastName} ${data.firstName}`,
        email: "email invalid",
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "email invalid",
    );
  });

  it("it should throw: phone required", async () => {
    const res = await request(app)
      .post(route)
      .send({
        name: `${data.lastName} ${data.firstName}`,
        email: data.email,
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "phone required",
    );
  });

  it("it should throw: phone invalid", async () => {
    const res = await request(app)
      .post(route)
      .send({
        name: `${data.lastName} ${data.firstName}`,
        email: data.email,
        phone: "0000",
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "phone invalid",
    );
  });

  it("it should throw: subject required", async () => {
    const res = await request(app)
      .post(route)
      .send({
        name: `${data.lastName} ${data.firstName}`,
        email: data.email,
        phone: data.phone,
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "subject required",
    );
  });

  it("it should throw: subject too long", async () => {
    const res = await request(app)
      .post(route)
      .send({
        name: `${data.lastName} ${data.firstName}`,
        email: data.email,
        phone: data.phone,
        subject: data.string256,
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "subject too long",
    );
  });

  it("it should throw: message required", async () => {
    const res = await request(app)
      .post(route)
      .send({
        name: `${data.lastName} ${data.firstName}`,
        email: data.email,
        phone: data.phone,
        subject: data.contactSubject,
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "message required",
    );
  });

  it("it should throw: message too long", async () => {
    const res = await request(app)
      .post(route)
      .send({
        name: `${data.lastName} ${data.firstName}`,
        email: data.email,
        phone: data.phone,
        subject: data.contactSubject,
        message: data.string10_001,
      });
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe(
      "message too long",
    );
  });

  it("it should create a contact request", async () => {
    const res = await request(app)
      .post(route)
      .send({
        name: `${data.lastName} ${data.firstName}`,
        email: data.email,
        phone: data.phone,
        subject: data.contactSubject,
        message:
          "Je suis un test pour prendre un rendez-vous du XX/XX à 9h jusqu'au XX/XX à 18h.\nCordialement",
      });
    expect(res.statusCode).toBe(200);
  });
});
