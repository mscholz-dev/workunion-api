import { Request, Response } from "express";
import EmailClass from "../utils/email/Email.js";
import ContactValidatorClass from "../utils/validator/ContactValidator.js";
import ContactServiceClass from "./contact.service.js";

// classes
const Email = new EmailClass();
const ContactValidator =
  new ContactValidatorClass();
const ContactService = new ContactServiceClass();

export default class ContactController {
  async create(req: Request, res: Response) {
    const schema =
      ContactValidator.inspectCreateData(
        req.body,
      );

    await ContactService.create(schema);

    await Email.contactCreateTemplate(schema);

    res.status(200).end();
  }
}
