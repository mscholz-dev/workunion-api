import { PrismaClient } from "@prisma/client";

// types
import { TContactCreateData } from "../utils/type.js";

// classes
const prisma = new PrismaClient();

export default class ContactService {
  async create({
    name,
    email,
    phone,
    subject,
    message,
  }: TContactCreateData): Promise<void> {
    await prisma.contact.create({
      data: {
        name,
        email,
        phone,
        subject,
        message,
      },
      select: {
        id: true,
      },
    });

    return;
  }
}
