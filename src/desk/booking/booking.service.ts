import { PrismaClient } from "@prisma/client";
import AppError from "../../utils/AppError.js";
import mongoose from "mongoose";

// types
import { TDeskBookingCreateData } from "../../utils/type.js";

// classes
const prisma = new PrismaClient();

export default class DeskBookingService {
  async create({
    desk_name,
    type,
    start,
    end,
    last_name,
    first_name,
    email,
    phone,
    company_name,
    company_siret,
  }: TDeskBookingCreateData) {
    const desk = await prisma.desk.findUnique({
      where: {
        name: desk_name,
      },
      select: { id: true },
    });

    if (!desk?.id)
      throw new AppError(
        "desk_name not exists",
        400,
      );

    const deskBookings =
      await prisma.desk_booking.findMany({
        where: {
          AND: [
            {
              desk_id: desk.id,
            },
            {
              OR: [
                {
                  start: {
                    lte: Number(end),
                    gte: Number(start),
                  },
                },
                {
                  end: {
                    lte: Number(end),
                    gte: Number(start),
                  },
                },
              ],
            },
          ],
        },
        select: {
          id: true,
        },
      });

    if (deskBookings.length)
      throw new AppError("date is taken", 400);

    const deskBooking =
      await prisma.desk_booking.create({
        data: {
          desk_id: desk.id,
          type,
          start: Number(start),
          end: Number(end),
          last_name,
          first_name,
          email,
          phone,
        },
        select: { id: true },
      });

    if (type !== "RENT") return deskBooking.id;

    await prisma.desk_company.create({
      data: {
        name: company_name as string,
        siret: company_siret as string,
        desk_booking_id: deskBooking.id,
      },
      select: { id: true },
    });

    return deskBooking.id;
  }

  async getDate(name: string) {
    const desk = await prisma.desk.findUnique({
      where: {
        name,
      },
      select: { id: true },
    });

    if (!desk?.id)
      throw new AppError(
        "name does not exist",
        400,
      );

    const bookings =
      await prisma.desk_booking.findMany({
        where: {
          desk_id: desk.id,
        },
        select: {
          start: true,
          end: true,
        },
      });

    return bookings;
  }

  async getId(name: string) {
    const desk = await prisma.desk.findUnique({
      where: {
        name,
      },
      select: { id: true },
    });

    if (!desk?.id)
      throw new AppError(
        "name does not exist",
        400,
      );

    const bookings =
      await prisma.desk_booking.findMany({
        where: {
          desk_id: desk.id,
        },
        select: {
          id: true,
          type: true,
        },
      });

    return bookings;
  }

  async delete(id: string) {
    if (!mongoose.Types.ObjectId.isValid(id))
      throw new AppError("id invalid", 400);

    const booking =
      await prisma.desk_booking.findUnique({
        where: {
          id,
        },
        select: {
          id: true,
          type: true,
          start: true,
        },
      });

    if (!booking)
      throw new AppError(
        "desk_booking not found",
        400,
      );

    const currentDate = new Date();
    currentDate.setDate(
      currentDate.getDate() + 2,
    );
    currentDate.setHours(9);

    const startDate = new Date(booking.start);

    if (currentDate >= startDate)
      throw new AppError(
        "deletion date exceeded",
        400,
      );

    await prisma.desk_booking.delete({
      where: {
        id,
      },
      select: {
        id: true,
      },
    });

    if (booking.type !== "RENT") return;

    await prisma.desk_company.delete({
      where: {
        desk_booking_id: booking.id,
      },
      select: {
        id: true,
      },
    });

    return;
  }
}
