import { Request, Response } from "express";
import EmailClass from "../../utils/email/Email.js";
import DeskBookingValidatorClass from "../../utils/validator/DeskBookingValidator.js";
import DeskBookingServiceClass from "./booking.service.js";
import SecurityClass from "../../utils/Security.js";
import AppError from "../../utils/AppError.js";

// classes
const DeskBookingValidator =
  new DeskBookingValidatorClass();
const DeskBookingService =
  new DeskBookingServiceClass();
const Email = new EmailClass();
const Security = new SecurityClass();

export default class DeskBookingController {
  async create(req: Request, res: Response) {
    const schema =
      DeskBookingValidator.inspectCreateData(
        req.body,
      );

    const bookingId =
      await DeskBookingService.create(schema);

    const newSchema = {
      ...schema,
      id: bookingId,
    };

    switch (newSchema.type) {
      case "COWORKING":
        await Email.bookingCreateCoworkingTemplate(
          newSchema,
        );
        break;

      case "RENT":
        await Email.bookingCreateRentTemplate(
          newSchema,
        );
        break;

      case "MEETING":
        await Email.bookingCreateMeetingTemplate(
          newSchema,
        );
        break;

      default:
        throw new AppError("type invalid", 400);
    }

    res.status(200).end();
  }

  async getDate(req: Request, res: Response) {
    // name required return a 404

    const bookings =
      await DeskBookingService.getDate(
        Security.xss(req.params.name),
      );

    res
      .status(200)
      .json({
        bookings,
      })
      .end();
  }

  async getId(req: Request, res: Response) {
    // name required return a 404

    const bookings =
      await DeskBookingService.getId(
        Security.xss(req.params.name),
      );

    res.status(200).json({ bookings }).end();
  }

  async delete(req: Request, res: Response) {
    // id required return a 404

    await DeskBookingService.delete(
      Security.xss(req.params.id),
    );

    res.status(200).end();
  }
}
