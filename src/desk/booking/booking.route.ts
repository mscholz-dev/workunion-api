import express, {
  Request,
  Response,
} from "express";
const router = express.Router();
import tryCatch from "../../utils/tryCatch.js";
import DeskBookingControllerClass from "./booking.controller.js";

// classes
const DeskBookingController =
  new DeskBookingControllerClass();

// route: create
router
  .route("/")
  .post(
    tryCatch(
      async (req: Request, res: Response) =>
        DeskBookingController.create(req, res),
    ),
  );

// route: get
router
  .route("/date/:name")
  .get(
    tryCatch(
      async (req: Request, res: Response) =>
        DeskBookingController.getDate(req, res),
    ),
  );

router
  .route("/id/:name")
  .get(
    tryCatch((req: Request, res: Response) =>
      DeskBookingController.getId(req, res),
    ),
  );

// route: delete
router
  .route("/:id")
  .delete(
    tryCatch(
      async (req: Request, res: Response) =>
        DeskBookingController.delete(req, res),
    ),
  );

export default router;
