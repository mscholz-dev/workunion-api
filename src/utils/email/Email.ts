import fs from "fs";
import nodemailer from "nodemailer";
import MomentClass from "../Moment.js";

// types
import {
  TContactCreateData,
  TDeskBookingCreateData,
} from "../type.js";

// classes
const Moment = new MomentClass();

export default class Email {
  deleteLink = `${process.env.BASE_URL_FRONT}/annulation-reservation`;

  send(
    emailDest: string,
    emailSubject: string,
    emailMessageHtml: string,
  ) {
    return new Promise(
      async (resolve, reject) => {
        const transporter =
          nodemailer.createTransport({
            host:
              String(process.env.MAILER_HOST) ||
              "",
            port:
              Number(process.env.MAILER_PORT) ||
              0,
            secure: true,
            auth: {
              user:
                String(process.env.MAILER_USER) ||
                "",
              pass:
                String(
                  process.env.MAILER_PASSWORD,
                ) || "",
            },
            service: "gmail",
          });

        const info = await transporter.sendMail({
          from: process.env.MAILER_USER,
          to: emailDest,
          subject: `WORKUNION • ${emailSubject}`,
          text: "",
          html: emailMessageHtml,
        });

        resolve(info);
      },
    );
  }

  async bookingCreateCoworkingTemplate({
    last_name,
    first_name,
    email,
    phone,
    desk_name,
    start,
    end,
    id,
  }: TDeskBookingCreateData) {
    const headTitle =
      "RÉSERVATION D'UN BUREAU DE COWORKING";

    const fileClient = fs
      .readFileSync(
        `./src/utils/email/fr/booking/create-coworking.client.html`,
      )
      .toString();

    const fileHtmlClient = fileClient
      .replace("$headTitle", headTitle)
      .replace("$lastName", last_name)
      .replace("$firstName", first_name)
      .replace("$email", email)
      .replace("$phone", phone)
      .replace("$deskName", desk_name)
      .replace(
        "$start",
        Moment.displayFullDate(start),
      )
      .replace(
        "$end",
        Moment.displayFullDate(end),
      )
      .replace(
        "$deleteLink",
        `${this.deleteLink}/${id}`,
      );

    await this.send(
      email,
      headTitle,
      fileHtmlClient,
    );

    const fileCompany = fs
      .readFileSync(
        `./src/utils/email/fr/booking/create-coworking.company.html`,
      )
      .toString();

    const fileHtmlCompany = fileCompany
      .replace("$headTitle", headTitle)
      .replace("$lastName", last_name)
      .replace("$firstName", first_name)
      .replace("$email", email)
      .replace("$phone", phone)
      .replace("$deskName", desk_name)
      .replace(
        "$start",
        Moment.displayFullDate(start),
      )
      .replace(
        "$end",
        Moment.displayFullDate(end),
      );

    await this.send(
      process.env.MAILER_USER || "",
      headTitle,
      fileHtmlCompany,
    );
  }

  async bookingCreateRentTemplate({
    last_name,
    first_name,
    email,
    phone,
    desk_name,
    start,
    end,
    company_name,
    company_siret,
    id,
  }: TDeskBookingCreateData) {
    const headTitle =
      "RÉSERVATION MENSUEL D'UN BUREAU";

    const fileClient = fs
      .readFileSync(
        `./src/utils/email/fr/booking/create-rent.client.html`,
      )
      .toString();

    const fileHtmlClient = fileClient
      .replace("$headTitle", headTitle)
      .replace("$lastName", last_name)
      .replace("$firstName", first_name)
      .replace("$email", email)
      .replace("$phone", phone)
      .replace(
        "$companyName",
        company_name as string,
      )
      .replace(
        "$companySiret",
        company_siret as string,
      )
      .replace("$deskName", desk_name)
      .replace(
        "$start",
        Moment.displayFullDate(start),
      )
      .replace(
        "$end",
        Moment.displayFullDate(end),
      )
      .replace(
        "$deleteLink",
        `${this.deleteLink}/${id}`,
      );

    await this.send(
      email,
      headTitle,
      fileHtmlClient,
    );

    const fileCompany = fs
      .readFileSync(
        `./src/utils/email/fr/booking/create-rent.company.html`,
      )
      .toString();

    const fileHtmlCompany = fileCompany
      .replace("$headTitle", headTitle)
      .replace("$lastName", last_name)
      .replace("$firstName", first_name)
      .replace("$email", email)
      .replace("$phone", phone)
      .replace(
        "$companyName",
        company_name as string,
      )
      .replace(
        "$companySiret",
        company_siret as string,
      )
      .replace("$deskName", desk_name)
      .replace(
        "$start",
        Moment.displayFullDate(start),
      )
      .replace(
        "$end",
        Moment.displayFullDate(end),
      );

    await this.send(
      process.env.MAILER_USER || "",
      headTitle,
      fileHtmlCompany,
    );
  }

  async bookingCreateMeetingTemplate({
    last_name,
    first_name,
    email,
    phone,
    desk_name,
    start,
    end,
    id,
  }: TDeskBookingCreateData) {
    const headTitle =
      "PRISE DE RENDEZ-VOUS POUR UN BUREAU";

    const fileClient = fs
      .readFileSync(
        `./src/utils/email/fr/booking/create-meeting.client.html`,
      )
      .toString();

    const fileHtmlClient = fileClient
      .replace("$headTitle", headTitle)
      .replace("$lastName", last_name)
      .replace("$firstName", first_name)
      .replace("$email", email)
      .replace("$phone", phone)
      .replace("$deskName", desk_name)
      .replace(
        "$start",
        Moment.displayFullDate(start),
      )
      .replace(
        "$end",
        Moment.displayFullDate(end),
      )
      .replace(
        "$deleteLink",
        `${this.deleteLink}/${id}`,
      );

    await this.send(
      email,
      headTitle,
      fileHtmlClient,
    );

    const fileCompany = fs
      .readFileSync(
        `./src/utils/email/fr/booking/create-meeting.company.html`,
      )
      .toString();

    const fileHtmlCompany = fileCompany
      .replace("$headTitle", headTitle)
      .replace("$lastName", last_name)
      .replace("$firstName", first_name)
      .replace("$email", email)
      .replace("$phone", phone)
      .replace("$deskName", desk_name)
      .replace(
        "$start",
        Moment.displayFullDate(start),
      )
      .replace(
        "$end",
        Moment.displayFullDate(end),
      );

    await this.send(
      process.env.MAILER_USER || "",
      headTitle,
      fileHtmlCompany,
    );
  }

  async contactCreateTemplate({
    name,
    email,
    phone,
    subject,
    message,
  }: TContactCreateData) {
    const headTitle = "PRISE DE CONTACT";

    const fileClient = fs
      .readFileSync(
        `./src/utils/email/fr/contact/create-contact.client.html`,
      )
      .toString();

    const fileHtmlClient = fileClient
      .replace("$headTitle", headTitle)
      .replace("$name", name)
      .replace("$email", email)
      .replace("$phone", phone)
      .replace("$subject", subject)
      .replace("$message", message);

    await this.send(
      email,
      headTitle,
      fileHtmlClient,
    );

    const fileCompany = fs
      .readFileSync(
        `./src/utils/email/fr/contact/create-contact.company.html`,
      )
      .toString();

    const fileHtmlCompany = fileCompany
      .replace("$headTitle", headTitle)
      .replace("$name", name)
      .replace("$email", email)
      .replace("$phone", phone)
      .replace("$subject", subject)
      .replace("$message", message);

    await this.send(
      process.env.MAILER_USER || "",
      headTitle,
      fileHtmlCompany,
    );
  }
}
