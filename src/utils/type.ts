export type TInspectData = {
  username?: string;
  email?: string;
  password?: string;
  password2?: string;
  id?: string;
  type?: string;
};

export type TDeskBookingCreateData = {
  desk_name: string;
  type: "COWORKING" | "RENT" | "MEETING";
  start: string;
  end: string;
  last_name: string;
  first_name: string;
  email: string;
  phone: string;
  company_name?: string;
  company_siret?: string;
  id?: string;
};

export type TDeskBookingGetData = {
  id: string;
};

export type TInspectDeleteData = {
  id: string;
};

export type TType =
  | "COWORKING"
  | "RENT"
  | "MEETING";

export type TContactCreateData = {
  name: string;
  email: string;
  phone: string;
  subject: string;
  message: string;
};
