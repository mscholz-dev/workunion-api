import Validator from "./Validator.js";
import RegexClass from "../Regex.js";

// types
import { TContactCreateData } from "../type.js";

// classes
const Regex = new RegexClass();

export default class ContactValidator extends Validator {
  inspectCreateData(data: TContactCreateData) {
    const schema = {
      name: "",
      email: "",
      phone: "",
      subject: "",
      message: "",
    };

    data = {
      ...data,
      phone: this.formatPhone(data.phone || ""),
    };

    this.inspectData(
      schema,
      data,
      this.errorMessage,
    );

    return schema as TContactCreateData;
  }

  errorMessage(
    id: string,
    value: string,
  ): string {
    switch (id) {
      // name
      case "name":
        if (!value) return "name required";
        if (value.length > 60)
          return "name too long";
        return "";

      // email
      case "email":
        if (!value) return "email required";
        if (value.length > 255)
          return "email too long";
        if (!Regex.email(value))
          return "email invalid";
        return "";

      // phone
      case "phone":
        if (!value) return "phone required";
        if (!Regex.phone(value))
          return "phone invalid";
        return "";

      // subject
      case "subject":
        if (!value) return "subject required";
        if (value.length > 255)
          return "subject too long";
        return "";

      // message
      case "message":
        if (!value) return "message required";
        if (value.length > 10_000)
          return "message too long";
        return "";

      // default
      default:
        return "error";
    }
  }
}
