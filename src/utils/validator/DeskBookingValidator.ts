import Validator from "./Validator.js";
import RegexClass from "../Regex.js";
import AppError from "../AppError.js";

// types
import { TDeskBookingCreateData } from "../type.js";

// classes
const Regex = new RegexClass();

export default class DeskBookingValidator extends Validator {
  inspectCreateData(
    data: TDeskBookingCreateData,
  ) {
    const schema = {
      desk_name: "",
      type: "",
      start: "",
      end: "",
      last_name: "",
      first_name: "",
      email: "",
      phone: "",
      company_name: "",
      company_siret: "",
    };

    data = {
      ...data,
      phone: this.formatPhone(data.phone || ""),
    };

    this.inspectData(
      schema,
      data,
      this.errorMessage,
    );

    this.checkType(
      schema as TDeskBookingCreateData,
    );

    this.checkDate(
      schema as TDeskBookingCreateData,
    );

    return schema as TDeskBookingCreateData;
  }

  inspectTypeData(type: string) {
    const schema = {
      type: "",
    };

    this.inspectData(
      schema,
      { type: type.toUpperCase() },
      this.errorMessage,
    );

    return schema.type;
  }

  checkType({
    type,
    company_name,
    company_siret,
  }: TDeskBookingCreateData) {
    if (type === "RENT" && !company_name)
      throw new AppError(
        "company_name required",
        400,
      );

    if (type === "RENT" && !company_siret)
      throw new AppError(
        "company_siret required",
        400,
      );
  }

  checkDate({
    start,
    end,
  }: TDeskBookingCreateData) {
    const startDate = new Date(Number(start));
    const endDate = new Date(Number(end));

    const minimalDay = new Date();
    minimalDay.setDate(minimalDay.getDate() + 1);
    minimalDay.setHours(23);

    if (startDate < minimalDay)
      throw new AppError(
        "start must be greater than or now + 2 days",
        400,
      );

    if (startDate >= endDate)
      throw new AppError(
        "start must be lower than end",
        400,
      );

    if (
      startDate.getHours() < 9 ||
      startDate.getHours() > 18
    )
      throw new AppError(
        "start must be between 9h and 18h",
        400,
      );

    if (
      endDate.getHours() < 9 ||
      endDate.getHours() > 18
    )
      throw new AppError(
        "end must be between 9h and 18h",
        400,
      );
  }

  errorMessage(
    id: string,
    value: string | number,
  ): string {
    switch (id) {
      // desk_name
      case "desk_name":
        if (!value) return "desk_name required";
        return "";

      // type
      case "type":
        if (!value) return "type required";
        if (
          value !== "COWORKING" &&
          value !== "RENT" &&
          value !== "MEETING"
        )
          return "type invalid";
        return "";

      // start
      case "start":
        if (!value) return "start required";
        if (isNaN(Number(value)))
          return "start must be a number";
        return "";

      // end
      case "end":
        if (!value) return "end required";
        if (isNaN(Number(value)))
          return "end must be a number";
        return "";

      // last_name
      case "last_name":
        if (!value) return "last_name required";
        if (
          typeof value === "string" &&
          value.length > 60
        )
          return "last_name too long";
        return "";

      // first_name
      case "first_name":
        if (!value) return "first_name required";
        if (
          typeof value === "string" &&
          value.length > 60
        )
          return "first_name too long";
        return "";

      // email
      case "email":
        if (!value) return "email required";
        if (
          typeof value === "string" &&
          value.length > 255
        )
          return "email too long";
        if (
          typeof value === "string" &&
          !Regex.email(value)
        )
          return "email invalid";
        return "";

      // phone
      case "phone":
        if (!value) return "phone required";
        if (
          typeof value === "string" &&
          !Regex.phone(value)
        )
          return "phone invalid";
        return "";

      // company_name
      case "company_name":
        return "";

      // company_siret
      case "company_siret":
        if (
          value &&
          typeof value === "string" &&
          !Regex.siret(value)
        )
          return "company_siret invalid";
        return "";

      // default
      default:
        return "error";
    }
  }
}
