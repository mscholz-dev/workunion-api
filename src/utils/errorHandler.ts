import AppError from "./AppError.js";
import {
  Request,
  Response,
  NextFunction,
} from "express";
import { PrismaClientKnownRequestError } from "@prisma/client/runtime/index.js";

const errorHandler = (
  err: {
    name?: string;
    details?: string;
    statusCode?: number;
  },
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  if (err instanceof AppError) {
    return res.status(err.statusCode).json({
      message: err.message,
    });
  }

  if (
    err instanceof PrismaClientKnownRequestError
  ) {
    switch (err.code) {
      case "P2025":
        return res.status(400).json({
          message:
            "record to delete does not exist",
        });

      default:
        return res.status(500).json({
          message: "Prisma error",
        });
    }
  }

  console.log(err);

  return res
    .status(500)
    .send("Something went wrong");
};

export default errorHandler;
