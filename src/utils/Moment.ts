import moment from "moment";
import "moment/locale/fr.js";
moment.locale("fr");

export default class Moment {
  displayFullDate(date: string) {
    const newDate = new Date(Number(date));

    return moment(newDate).format(
      "dddd DD MMMM à HH:mm",
    );
  }
}
